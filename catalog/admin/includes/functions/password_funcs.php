<?php
/*
$Id$

  osCmax e-Commerce
  http://www.oscmax.com

  Copyright 2000 - 2011 osCmax

  Released under the GNU General Public License
*/

require DIR_WS_FUNCTIONS . 'password_hash.php';
////
// This function compares a plain text password with an encrpyted password
  function tep_validate_password($plain, $encrypted) {
    if (tep_not_null($plain) && tep_not_null($encrypted)) {
      // BOF: Secure Password Hash Mod by SMurphy
            // If the password hash is the old style
            if (strlen($encrypted) == 35 && substr($encrypted, 32, 1) == ':') {
      // split apart the hash / salt
              $stack = explode(':', $encrypted);

              if (sizeof($stack) != 2) return false;

              if (md5($stack[1] . $plain) == $stack[0]) {
                return 'rehash';
              }
            } else {
              if (password_verify($plain, $encrypted)) {
                if (password_needs_rehash($encrypted, PASSWORD_DEFAULT)) { 
                  return 'rehash';
                }

                return true;
              }
            }
      // EOF: Secure Password Hash Mod by SMurphy
    }

    return false;
  }

////
// This function makes a new encrypted password from a plain text password.
  function tep_encrypt_password($plain) {
    $password = '';

    // BOF: Secure Password Hash Mod by SMurphy
        $password = password_hash($plain, PASSWORD_DEFAULT);
    // EOF: Secure Password Hash Mod by SMurphy

    return $password;
  }
?>
