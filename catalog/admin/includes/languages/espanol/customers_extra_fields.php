<?php
/*
  $Id: extra_fields.php, v1.2 2002/08/19 01:58:58 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Campos extra de clientes');

define('TABLE_HEADING_FIELDS', 'Campos');
define('TABLE_HEADING_CEF_CG_HIDE', 'Grupo oculto de clientes');
define('TABLE_HEADING_ACTION', 'Acción');

define('TEXT_HEADING_NEW_FIELD', 'Nuevo Campo');
define('TEXT_HEADING_EDIT_FIELD', 'Editar Campo');
define('TEXT_HEADING_DELETE_FIELD', 'Borrar Campo');
define('TABLE_HEADING_STATUS','Estado');


define('TEXT_FIELD', 'Campo: ');
define('TEXT_FIELD_INPUT_TYPE', 'Tipo de entrada: ');
define('TEXT_FIELD_INPUT_VALUE', 'Valores de entrada:<br>(cada linea es como un nuevo elemento por comprobar/lista radio) ');
define('TEXT_FIELD_REQUIRED_STATUS', 'Estado requerido:<br>(recuerde ajustar el valor mínimo/tamaño)');
define('TEXT_FIELD_REQUIRED_EMAIL', 'Campo enviado en email: ');
define('TEXT_FIELD_STATUS_EMAIL', 'Enviar compo en emails: ');
define('TEXT_INPUT_FIELD','Introducir campo');
define('TEXT_TEXTAREA_FIELD','Campo área de texto');
define('TEXT_RADIO_FIELD','Lista Botones de Radio');
define('TEXT_CHECK_FIELD','Casilla de multi verificación');
define('TEXT_DOWN_FIELD','Menú desplegable');
define('TEXT_FIELD_SIZE', 'Campo mínimo valor/tamaño: ');
define('TEXT_NEW_INTRO', 'Por favor complete la siguiente información para el nuevo campo');
define('TEXT_EDIT_INTRO', 'Por favor haga los cambios necesarios');
define('TEXT_FIELD_NAME', 'Nombre del campo: ');
define('TEXT_DELETE_INTRO', 'Está seguro que desea eliminar este campo?');
define('TEXT_CEF_CG_HIDE', 'Ocultar grupo de cliente(s): ');
?>