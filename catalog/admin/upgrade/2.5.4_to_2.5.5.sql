/*
/* osCmax v2.5.4 to 2.5.5 Incremental Database Upgrade */
/**********************************************************************/
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/* Bugfix #1300 - Update to secure password hashing from obsolete MD5 */
ALTER TABLE `admin` CHANGE `admin_password` `admin_password` VARCHAR( 255 );
ALTER TABLE `affiliate_affiliate` CHANGE `affiliate_password` `affiliate_password` VARCHAR( 255 );
ALTER TABLE `customers` CHANGE `customers_password` `customers_password` VARCHAR( 255 );

SET AUTOCOMMIT = 0;


/* SYNC TABLE : db_version */

UPDATE db_version SET database_version='v2.5.5';


COMMIT;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
